# S3 Bucket using CDK with AWS 
Tianji Rao

## Introduction
The project's objective is to harness the principles of Infrastructure as Code (IaC) to systematically provision and manage an Amazon S3 bucket. This endeavor marks a foundational step towards enhancing our deployment strategy for AWS services, circumventing the manual intricacies and potential inconsistencies associated with using the AWS Management Console. By using the S3 bucket within codes, we can do streamlined deployment, version control, cost management.


## Steps
To deploy an S3 bucket using AWS CDK with Codecatalyst and ensure proper AWS configurations, follow these summarized steps:

1. Access Codecatalyst: Ensure you have access to Codecatalyst, utilizing the free tier if available. 
2. Connect Builder ID: Link a builder ID which is necessary for AWS Codewhisperer and Codecatalyst integration. 
3. Set Up Development Environment: Create an empty development environment to work within. 
4. Verify CDK Installation: Check if AWS CDK is installed by running cdk --version in your development environment. If not installed, install it.
5. Create IAM User: Navigate to IAM in AWS Console to create a new user. Assign IAMFullAccess and AmazonS3FullAccess policies directly. 
6. Add Inline Policies: 
- For CloudFormation: Add an inline policy granting full access to all CloudFormation commands. 
- For Systems Manager: Similarly, add another inline policy for full access to Systems Manager commands. 
7. Generate Access Keys: Create an access key ID and secret access key for the new IAM user. Store these credentials securely. 
8.Configure AWS CLI: In your development environment, run aws configure to input your AWS access key ID and secret access key. Optionally set the region but be mindful it may impact later steps. 
9. Initialize CDK Project: Create a new directory for your project, navigate into it, and initialize a new CDK app specifying TypeScript as the language with cdk init app --language=typescript. 
10. Develop Your Project: Utilize the CDK to define your S3 bucket resource within the TypeScript project. Ensure your project builds correctly with npm run build. 
11. Synthesize CloudFormation Template: Generate the CloudFormation template for your infrastructure with cdk synth. 
12. Deploy with CDK: Attempt to deploy your infrastructure using cdk deploy. If deployment fails due to a missing role, follow the steps to create and attach the necessary role. 
- Handle Deployment Failure: If there's a failure, capture the role name from the error message. Create a role using the captured name and the zombie.json policy document. Attach the AdministratorAccess policy to this role. 
- Retry Deployment: With the role configured, reattempt the deployment with cdk deploy. 
15. Verify Deployment: Check the AWS S3 console to confirm your bucket has been deployed successfully.


## S3 Bucket
**S3 bucket**
![Screenshot_2024-02-10_at_5.38.14_PM](/uploads/744e08e4335299340d1d541aa1866189/Screenshot_2024-02-10_at_5.38.14_PM.png)

**Enabled**
![Screenshot_2024-02-10_at_5.39.24_PM](/uploads/613cad95ac1f2445e6980008a80210ce/Screenshot_2024-02-10_at_5.39.24_PM.png)


## Codewhisper
In this project, AWS Codewhisperer played a crucial role in accelerating the development process and enhancing the efficiency of writing infrastructure as code (IaC) for provisioning an Amazon S3 bucket using AWS CDK. By integrating AWS Codewhisperer within our development environment, we were able to leverage its AI-powered code suggestions to swiftly generate accurate and optimized code snippets for defining AWS resources, particularly the S3 bucket configurations. This not only significantly reduced the manual coding effort but also minimized the potential for errors and inconsistencies that could arise from manual code writing. Codewhisperer's ability to understand the context of our project and provide relevant code recommendations based on AWS best practices ensured that our infrastructure code was robust, secure, and aligned with AWS standards. This integration facilitated a more streamlined and efficient deployment process, allowing us to focus on strategic tasks and project objectives while Codewhisperer handled the intricacies of code generation and optimization.